<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/','App\Http\Controllers\MahasiswaController@index');
Route::get('/home','App\Http\Controllers\MahasiswaController@index');
Route::get('/tambah','App\Http\Controllers\MahasiswaController@tambah');
Route::post('/addMhs','App\Http\Controllers\MahasiswaController@addMhs');


