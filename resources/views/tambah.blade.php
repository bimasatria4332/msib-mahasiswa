<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
    <div class="row">
        <div class="col-12">
            <nav class="navbar navbar-expand-lg navbar-light bg-dark">
                <div class="container-fluid">
                    <a class="navbar-brand text-light" href="/">Coba Mahasiswa</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active text-light" aria-current="page" href="/">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-light" href="/tambah">Tambah</a>
                        </li>
                    </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>

    <div class="row justify-content-center mt-4">
        <div class="col-8">
            <form action="/addMhs" method="post">
                @csrf
                <div class="input-group mb-3">
                    <span class="input-group-text" id="basic-addon1">NIM</span>
                    <input type="number" class="form-control" name="nim" placeholder="nim" aria-label="NIM" maxlength=12 aria-describedby="basic-addon1" required>
                </div>
                <div class="input-group mb-3">
                    <span class="input-group-text" id="basic-addon1">Nama</span>
                    <input type="text" class="form-control" name="nama" placeholder="nama" aria-label="Nama" maxlength=100 aria-describedby="basic-addon1" required>
                </div>
                <div class="input-group mb-3">
                    <span class="input-group-text" id="basic-addon1">Fakultas</span>
                    <input type="text" class="form-control" name="fakultas" placeholder="fakultas" aria-label="Fakultas" maxlength=100 aria-describedby="basic-addon1" required>
                </div>
                <div class="input-group mb-3">
                    <span class="input-group-text" id="basic-addon1">Prodi</span>
                    <input type="text" class="form-control" name="prodi" placeholder="prodi" aria-label="Prodi" maxlength=100 aria-describedby="basic-addon1" required>
                </div>

                <div class="form-check">
                    <input class="form-check-input" type="radio" name="jenis_kelamin" value="pria" id="flexRadioDefault1" required>
                    <label class="form-check-label" for="flexRadioDefault1">Pria</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="jenis_kelamin" value="wanita" id="flexRadioDefault1" required>
                    <label class="form-check-label" for="flexRadioDefault1">Wanita</label>
                </div>

                <button class="btn btn-primary w-100 mt-5" type="submit">Tambah</button>
            </form>
        </div>

    </div>
        


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>