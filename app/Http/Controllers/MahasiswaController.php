<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\MahasiswaModel;


class MahasiswaController extends Controller
{
    public function index()
    {
        $data['title'] = "Daftar MHS";
        $data['students'] =  MahasiswaModel::all(); 
 
        return view('mahasiswa-all', $data);

    }
    public function tambah()
    {
        $data['title'] = "Tambah MHS";

        
        return view('tambah', $data);

    }
    public function addMhs(Request $request)
    {
        $data['title'] = "Daftar MHS";
        $post = MahasiswaModel::create([
            'nim'    => $request->nim,
            'nama' => $request->nama,
            'fakultas' => $request->fakultas,
            'prodi' => $request->prodi,
            'jenis_kelamin' => $request->jenis_kelamin,
        ]);
        
        return redirect()->to('home');
    }
}
