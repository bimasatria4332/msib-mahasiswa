<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MahasiswaModel extends Model
{
    
    use HasFactory;

    
    protected $primaryKey = 'nim';


    public $timestamps  = false;
    protected $table    = 'mahasiswa';
    protected $fillable = ['nim','nama', 'fakultas', 'prodi', 'fakultas', 'jenis_kelamin'];
}
